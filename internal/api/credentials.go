package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
)

func CredentialRoutes(group *gin.RouterGroup, e common.Env) *gin.RouterGroup {

	credentialGroup := group.Group("/credentials")

	credentialGroup.GET("/list", common.ConstructResponse(handlers.ListCredentials, e))
	credentialGroup.POST("/list", common.ConstructResponse(handlers.ListCredentials, e))

	credentialGroup.GET("/history", common.ConstructResponse(handlers.ListCredentials, e))

	credentialGroup.DELETE("/:id", common.ConstructResponse(handlers.DeleteCredential, e))

	credentialGroup.PUT("/issue", common.ConstructResponse(handlers.IssueCredential, e))

	credentialGroup.PUT("/backup/:id/:bid", common.ConstructResponse(handlers.CreateBackupCredentials, e))
	credentialGroup.GET("/backup/:id/:bid", common.ConstructResponse(handlers.GetBackupCredentials, e))
	credentialGroup.GET("/backup/link/:mode", common.ConstructResponse(handlers.GenerateBackupLink, e))
	credentialGroup.GET("/backup/all", common.ConstructResponse(handlers.GetAllBackupCredentials, e))
	credentialGroup.GET("/backup/latest", common.ConstructResponse(handlers.GetLastBackupCredentials, e))
	credentialGroup.DELETE("/backup/invalid", common.ConstructResponse(handlers.DeleteInvalidUserBackups, e))
	credentialGroup.DELETE("/backup/:bid", common.ConstructResponse(handlers.DeleteBackup, e))

	credentialGroup.GET("/:id/revoke", common.ConstructResponse(handlers.RevokeCredential, e))

	return credentialGroup
}
