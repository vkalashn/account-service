package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
)

func HistoryRoutes(group *gin.RouterGroup, e common.Env) *gin.RouterGroup {
	historyGroup := group.Group("/history")

	historyGroup.GET("/list", common.ConstructResponse(handlers.ListHistory, e))

	return historyGroup
}
