package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
)

func SettingsRoutes(group *gin.RouterGroup, e common.Env) *gin.RouterGroup {

	settingsGroup := group.Group("/settings")

	settingsGroup.GET("/ui", common.ConstructResponse(handlers.GetUiSettings, e))

	settingsGroup.POST("/ui", common.ConstructResponse(handlers.SetUiSettings, e))

	return settingsGroup
}
