package common

type ProtocolType string

const (
	Nats ProtocolType = "nats"
)

type DatabaseType string

const (
	Postgres DatabaseType = "postgres"
)

type DataKey string

const (
	UserKey DataKey = "user"
)

const ModeDownload = "download"
const ModeUpload = "upload"

const (
	Consent             RecordEventType = "consent"
	Pairing             RecordEventType = "pairing"
	Issued              RecordEventType = "issued"
	Presented           RecordEventType = "presented"
	Revoked             RecordEventType = "revoked"
	PresentationRequest RecordEventType = "presentationRequest"
)

func RecordEventTypes() []RecordEventType {
	return []RecordEventType{Consent, Pairing, Issued, Presented, Revoked, PresentationRequest}
}
