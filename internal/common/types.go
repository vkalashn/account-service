package common

import (
	"github.com/Nerzal/gocloak/v13"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gorm.io/gorm"
	"net/http"
)

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type UserInfo struct {
	*gocloak.UserInfo
}

func (u *UserInfo) ID() string {
	sub := u.UserInfo.Sub
	return *sub
}

type Env interface {
	IsHealthy() bool
	GetDB() *gorm.DB
	GetBroker(topic string) *cloudeventprovider.CloudEventProviderClient
	GetCryptoProvider() types.CryptoProvider
	GetNamespace() string
	AddBroker(topic string, handler func(e event.Event))
	GetRandomId() string
	GetHttpClient() HttpClient
}

type EndpointHandler func(*gin.Context, Env) (any, error)

type EventHandler func(event.Event, Env) error

type HistoryRecorder func(EndpointHandler, *gin.Context, Env, RecordEventType, string) (any, error)

type RecordEventType string

type RecordedFunc func(...any) error
