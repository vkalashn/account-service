package config

import (
	"reflect"
	"strings"
	"time"

	"github.com/spf13/viper"
	cloud "gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core"
)

const (
	EnvVarPrefix    = "ACCOUNT"
	EnvVarSeparator = "_"
)

type TemplateConfiguration struct {
	core.BaseConfig `mapstructure:",squash"`

	Name string `mapstructure:"serviceName"`

	Protocol cloud.ProtocolType `mapstructure:"protocol,omitempty"`

	Nats struct {
		WithNats     bool          `mapstructure:"withNats"`
		Url          string        `mapstructure:"url"`
		QueueGroup   string        `mapstructure:"queueGroup,omitempty"`
		TimeoutInSec time.Duration `mapstructure:"timeoutInSec,omitempty"`
	} `mapstructure:"nats"`

	Database struct {
		WithDB   bool   `mapstructure:"withDB,omitempty"`
		DBType   string `mapstructure:"dbType,omitempty"` // common.DatabaseType
		Host     string `mapstructure:"host,omitempty"`
		Port     int    `mapstructure:"port,omitempty"`
		User     string `mapstructure:"user,omitempty"`
		Password string `mapstructure:"password,omitempty"`
		DBName   string `mapstructure:"dbName,omitempty"`
	} `mapstructure:"db,omitempty"`

	CloudEvents struct {
		Topics []string `mapstructure:"topics"`
	} `mapstructure:"cloudevents,omitempty"`

	KeyCloak struct {
		Url              string        `mapstructure:"url"`
		Login            string        `mapstructure:"login"`
		Password         string        `mapstructure:"password"`
		RealmName        string        `mapstructure:"realmName"`
		TokenTTL         time.Duration `mapstructure:"tokenTTL"`
		ExcludeEndpoints string        `mapstructure:"excludeEndpoints"`
	} `mapstructure:"keycloak,omitempty"`

	Storage struct {
		Url      string `mapstructure:"url"`
		KeyPath  string `mapstructure:"keyPath"`
		WithAuth bool   `mapstructure:"withAuth"`
	} `mapstructure:"storage,omitempty"`

	BackupLinkTTL      time.Duration `mapstructure:"backupLinkTTL"`
	CredentialVerifier struct {
		Url string `mapstructure:"url"`
	} `mapstructure:"credentialVerifier,omitempty"`

	DIDComm struct {
		Url string `mapstructure:"url"`
	} `mapstructure:"didcomm,omitempty"`

	Signer struct {
		Url string `mapstructure:"url"`
	} `mapstructure:"signer,omitempty"`
}

var ServerConfiguration TemplateConfiguration

func LoadConfig() error {
	var err error
	loader := viper.NewWithOptions(viper.KeyDelimiter(EnvVarSeparator))
	loader.SetConfigFile("config.yaml")
	loader.SetEnvPrefix(EnvVarPrefix)
	loader.AutomaticEnv()
	if err != nil {
		return err
	}
	err = loader.ReadInConfig()
	if err != nil {
		return err
	}

	envVarNames := make([]string, 0)
	envVarNames = getConfigFieldsAsEnvNames(TemplateConfiguration{}, "", EnvVarSeparator, envVarNames)
	for _, envVarName := range envVarNames {
		loader.SetDefault(envVarName, "") // required for config to read env variables due to viper defect
	}
	for key, value := range getDefaults() {
		loader.SetDefault(key, value)
	}
	err = loader.Unmarshal(&ServerConfiguration)
	if err != nil {
		return err
	}
	return nil
}

func getDefaults() map[string]any {
	return map[string]any{
		"logLevel": "info",
		"isDev":    true,
		"port":     8080,
	}
}

func getConfigFieldsAsEnvNames(config interface{}, prefix string, sep string, acc []string) []string {
	// required to get env var names from config
	t := reflect.TypeOf(config)
	v := reflect.ValueOf(config)
	fields := reflect.VisibleFields(t)
	for _, field := range fields {
		if !field.IsExported() {
			continue
		} else if field.Type.Kind() == reflect.Struct {
			tag := field.Tag.Get("mapstructure")
			tag = cleanTag(tag)
			newPrefix := prefix + tag + sep
			fieldValue := v.FieldByName(field.Name).Interface()
			acc = append(acc, getConfigFieldsAsEnvNames(fieldValue, newPrefix, sep, acc)...)
		} else {
			tag := field.Tag.Get("mapstructure")
			tag = cleanTag(tag)
			acc = append(acc, prefix+tag)
		}
	}
	return acc
}

func cleanTag(tag string) string {
	var dropSymbols = []string{",", "omitempty", "squash"}

	for _, sym := range dropSymbols {
		tag = strings.ReplaceAll(tag, sym, "")
	}
	return tag
}
