package env

import (
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/google/uuid"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/connection"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/database"
	"gorm.io/gorm"
	"net/http"
	"os"
	"sync"
)

const (
	AccountServiceNamespace = "accountSpace"
)

var logger = common.GetLogger()

var env *EnvObj

var cryptoProvider types.CryptoProvider

type EnvObj struct {
	db      *gorm.DB
	brokers map[string]*cloudeventprovider.CloudEventProviderClient
}

func (env *EnvObj) IsHealthy() bool {
	db, err := env.db.DB()
	if err == nil {
		err = db.Ping()
	}
	return err == nil
}

func (env *EnvObj) GetDB() *gorm.DB {
	if env.db == nil {
		logger.Error(nil, "database connection was not initialised")
		return &gorm.DB{}
	}
	return env.db
}

func (env *EnvObj) GetBroker(topic string) *cloudeventprovider.CloudEventProviderClient {
	return env.brokers[topic]
}

var onceCrypt sync.Once

func (env *EnvObj) GetCryptoProvider() types.CryptoProvider {
	onceCrypt.Do(initCrypto)
	return cryptoProvider
}

func (env *EnvObj) GetNamespace() string {
	return AccountServiceNamespace
}
func (env *EnvObj) AddBroker(topic string, handler func(e event.Event)) {
	if config.ServerConfiguration.Nats.WithNats {
		broker, subscribe, err := connection.CloudEventsConnection(topic, handler)
		if err != nil {
			logger.Error(err, "subscription failed", "topic", topic)
			return
		}
		env.brokers[topic] = broker
		go func() {
			er := subscribe()
			if er != nil {
				logger.Error(er, "subscription failed", "topic", topic)
			}
		}()
	}
}

func (env *EnvObj) GetRandomId() string {
	return uuid.New().String()
}

func (env *EnvObj) GetHttpClient() common.HttpClient {
	return http.DefaultClient
}

var onceEnv sync.Once

func GetEnv() common.Env {
	onceEnv.Do(initEnv)
	return env
}

func initEnv() {
	envir := DefaultEnv()
	if config.ServerConfiguration.Database.WithDB {
		db, err := database.NewDatabaseConnection(common.DatabaseType(config.ServerConfiguration.Database.DBType))
		if err != nil {
			logger.Error(err, "connection to database failed")
			os.Exit(1)
		} else {
			envir.db = db
		}
	}

	env = &envir
}

func initCrypto() {
	cryptoProvider = core.CryptoEngine()
}

func DefaultEnv() EnvObj {
	envir := EnvObj{
		db:      nil,
		brokers: make(map[string]*cloudeventprovider.CloudEventProviderClient),
	}
	return envir
}
