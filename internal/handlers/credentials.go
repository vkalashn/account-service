package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/presentation"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/model"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/services"
	"gorm.io/gorm"
	"io"
	"net/url"
	"time"
)

var logger = common.GetLogger()

type ListCredentialRequestBody struct {
	Search string `json:"search"`
}

func ListCredentials(ctx *gin.Context, e common.Env) (any, error) {
	stor := services.GetStorage(e.GetHttpClient())
	user, err := common.GetUserFromContext(ctx)
	auth := ctx.Request.Header.Get("Authorization")
	if err != nil {
		return nil, err
	} else {
		var requestBody ListCredentialRequestBody
		jsonData, err := io.ReadAll(ctx.Request.Body)
		if err != nil {
			return nil, common.ErrorResponseBadRequest(ctx, "cannot parse the body", nil)
		}

		if len(jsonData) != 0 {
			err = json.Unmarshal(jsonData, &requestBody)
			if err != nil {
				return nil, common.ErrorResponseBadRequest(ctx, "cannot parse the json body", nil)
			}
		}
		if requestBody.Search != "" {
			constraints := buildConstraints(requestBody.Search)
			return stor.GetCredentials(auth, user.ID(), constraints)
		}
		return stor.GetCredentials(auth, user.ID(), nil)
	}
}

func ListPresentations(ctx *gin.Context, e common.Env) (any, error) {
	stor := services.GetStorage(e.GetHttpClient())
	tmp := ctx.Request.Context().Value(common.UserKey)
	auth := ctx.Request.Header.Get("Authorization")
	if user, ok := tmp.(*common.UserInfo); !ok {
		return nil, common.ErrorResponseBadRequest(ctx, "cannot extract user data from request context", nil)
	} else {
		var requestBody ListCredentialRequestBody
		jsonData, err := io.ReadAll(ctx.Request.Body)
		if err != nil {
			return nil, common.ErrorResponseBadRequest(ctx, "cannot parse the body", nil)
		}

		if len(jsonData) != 0 {
			err = json.Unmarshal(jsonData, &requestBody)
			if err != nil {
				return nil, common.ErrorResponseBadRequest(ctx, "cannot parse the json body", nil)
			}
		}
		if requestBody.Search != "" {
			constraints := buildConstraints(requestBody.Search)
			return stor.GetPresentations(auth, user.ID(), constraints)
		}
		return stor.GetPresentations(auth, user.ID(), nil)
	}
}

func ListCredentialHistory(ctx *gin.Context) (any, error) {
	mock := make(map[string]string)
	mock2 := make(map[string]interface{})
	mock2["credentials"] = mock
	mock["name"] = "My Device"
	mock["connectionId"] = "4729423498hsdfndfj3jsjsj"
	mock["date"] = "12.12.23"
	mock["issuedTo"] = "..."
	return mock, nil
}

func DeleteCredential(ctx *gin.Context, e common.Env) (any, error) {
	mock := make(map[string]string)
	return mock, nil
}

func IssueCredential(ctx *gin.Context, e common.Env) (any, error) {
	mock := make(map[string]string)
	return mock, nil
}

func RevokeCredential(ctx *gin.Context, e common.Env) (any, error) {
	mock := make(map[string]string)
	return mock, nil
}

func buildConstraints(searchValue string) *presentation.PresentationDefinition {

	searchString := fmt.Sprintf("$.credentialSubject[?(@ =~ /%s/)]", searchValue)
	field := presentation.Field{
		Path: []string{searchString},
	}
	constraints := presentation.Constraints{
		LimitDisclosure: "",
		Fields:          []presentation.Field{field},
	}
	inputDescriptor := presentation.InputDescriptor{
		Description: presentation.Description{},
		Format:      presentation.Format{},
		Constraints: constraints,
		Group:       []string{},
	}
	result := &presentation.PresentationDefinition{
		InputDescriptors: []presentation.InputDescriptor{inputDescriptor},
		Format:           presentation.Format{LDPVC: &presentation.FormatSpecification{}},
	}
	return result
}

type Backup struct {
	BindingId   string    `json:"bindingId,omitempty"`
	UserId      string    `json:"user_id"`
	Name        string    `json:"name"`
	Credentials []byte    `json:"credentials"`
	Timestamp   time.Time `json:"timestamp"`
}

type GetBackupsOutput struct {
	Backups []Backup `json:"backups"`
}

type BackupLinkOutput struct {
	Path string  `json:"path"`
	TTL  float64 `json:"expiresInSeconds"`
}

func fromModelBackupToOutputBackup(from model.Backup) Backup {
	return Backup{
		BindingId: from.BindingId.String, UserId: from.UserId, Credentials: from.Credentials, Timestamp: from.CreatedAt,
	}
}

func CreateBackupCredentials(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	bid := ctx.Param("bid")
	if bid == "" {
		return "", common.ErrorResponseBadRequest(ctx, "url param `bid` required", nil)
	}
	data, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		return nil, err
	}
	crCtx := types.CryptoContext{
		Namespace: e.GetNamespace(),
		Group:     "account",
		Context:   context.Background(),
	}

	keyId := types.CryptoIdentifier{
		KeyId:         user.ID(),
		CryptoContext: crCtx,
	}

	var er error
	var exists bool

	if exists, er = e.GetCryptoProvider().IsCryptoContextExisting(crCtx); er == nil && !exists {
		er = e.GetCryptoProvider().CreateCryptoContext(crCtx)
	}
	if er != nil {
		return nil, er
	}

	if exists, er = e.GetCryptoProvider().IsKeyExisting(keyId); er == nil && !exists {
		er = e.GetCryptoProvider().GenerateKey(types.CryptoKeyParameter{
			Identifier: keyId,
			KeyType:    types.Aes256GCM,
		})
	}
	if er != nil {
		return nil, er
	}
	data, err = e.GetCryptoProvider().Encrypt(keyId, data)
	if err != nil {
		return nil, err
	}
	return "", model.EnrichBackupDBEntry(e.GetDB(), bid, data)
}

func GetAllBackupCredentials(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	var after time.Time
	if tmp := ctx.Query("after"); tmp != "" {
		after, err = time.Parse(time.UnixDate, tmp)
	}
	if err != nil {
		return nil, err
	}
	backups, err := model.GetBackups(e.GetDB(), user.ID(), after)
	if err != nil {
		return nil, err
	}
	keyId := types.CryptoIdentifier{
		KeyId: user.ID(),
		CryptoContext: types.CryptoContext{
			Namespace: e.GetNamespace(),
			Group:     "account",
			Context:   context.Background(),
		},
	}
	var res = GetBackupsOutput{Backups: make([]Backup, 0)}
	for _, b := range backups {
		if b.Credentials != nil && len(b.Credentials) > 0 {
			b.Credentials, err = e.GetCryptoProvider().Decrypt(keyId, b.Credentials)
			if err != nil {
				return nil, err
			}
		}
		res.Backups = append(res.Backups, fromModelBackupToOutputBackup(b))
	}
	return res, nil
}

func GetBackupCredentials(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	bid := ctx.Param("bid")
	if bid == "" {
		return "", common.ErrorResponseBadRequest(ctx, "url param `bid` required", nil)
	}

	backups, err := model.GetBackup(e.GetDB(), bid)
	if err != nil {
		return nil, err
	}
	keyId := types.CryptoIdentifier{
		KeyId: user.ID(),
		CryptoContext: types.CryptoContext{
			Namespace: e.GetNamespace(),
			Group:     "account",
			Context:   context.Background(),
		},
	}

	var res = GetBackupsOutput{Backups: make([]Backup, 0)}
	for _, b := range backups {
		b.Credentials, err = e.GetCryptoProvider().Decrypt(keyId, b.Credentials)
		if err != nil {
			return nil, err
		}
		res.Backups = append(res.Backups, fromModelBackupToOutputBackup(b))
	}
	return res, nil
}

func GetLastBackupCredentials(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	backup, err := model.GetLastBackup(e.GetDB(), user.ID())
	if err != nil {
		return nil, err
	}
	keyId := types.CryptoIdentifier{
		KeyId: user.ID(),
		CryptoContext: types.CryptoContext{
			Namespace: e.GetNamespace(),
			Group:     "account",
			Context:   context.Background(),
		},
	}
	backup.Credentials, err = e.GetCryptoProvider().Decrypt(keyId, backup.Credentials)
	return GetBackupsOutput{Backups: []Backup{fromModelBackupToOutputBackup(backup)}}, nil
}

func GenerateBackupLink(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}

	mode := ctx.Param("mode")
	name := ctx.Query("name")
	var bid string
	var dbOperation func(*gorm.DB, string, string, string, []byte) error

	if mode == common.ModeUpload {
		bid = e.GetRandomId()
		dbOperation = model.CreateBackupDBEntry
	} else if mode == common.ModeDownload {
		bid = ctx.Query("bindingId")
		if bid == "" {
			return nil, common.ErrorResponseBadRequest(ctx, "queryParam `bindingId` required", nil)
		}
		dbOperation = func(db *gorm.DB, s string, s2 string, s3 string, bytes []byte) error {
			return nil
		}
	} else {
		return nil, fmt.Errorf("unknown backup link mode")
	}

	path := getOneTimePath(ctx, bid, user)
	res := BackupLinkOutput{Path: path.String(), TTL: config.ServerConfiguration.BackupLinkTTL.Seconds()}
	return res, dbOperation(e.GetDB(), bid, user.ID(), name, []byte{})
}

func getOneTimePath(ctx *gin.Context, bid string, user *common.UserInfo) *url.URL {
	path := &url.URL{}
	if ctx.Request.TLS == nil {
		path.Scheme = "http"
	} else {
		path.Scheme = "https"
	}
	path.Host = ctx.Request.Host
	path.Path = fmt.Sprintf("api/accounts/credentials/backup/%s/%s", user.ID(), bid)
	path.RawQuery = url.Values{}.Encode()
	return path
}

func DeleteBackup(ctx *gin.Context, e common.Env) (any, error) {
	bid := ctx.Param("bid")
	return "", model.DeleteBackupById(e.GetDB(), bid)
}

func DeleteInvalidUserBackups(ctx *gin.Context, e common.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	return "", model.DeleteInvalidatedBackups(e.GetDB(), user.ID())
}
