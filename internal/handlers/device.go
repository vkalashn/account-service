package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	cmn "gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/services"
)

const (
	invitationEventType = "device.connection"
	invitationTopic     = "remotecontrol.xxx"
	invitationProtocol  = "nats"
)

func ListDevices(ctx *gin.Context, e cmn.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	didcomm := services.GetDIDComm(nil)

	list, err := didcomm.GetConnectionList(user.ID(), ctx.Query("search"))

	if err != nil {
		return nil, err
	}

	return list, nil
}

func LinkDevice(ctx *gin.Context, e cmn.Env) (any, error) {
	user, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	didcomm := services.GetDIDComm(nil)

	reqBody := services.InvitationRequestBody{
		Protocol:  invitationProtocol,
		Topic:     invitationTopic,
		Group:     user.ID(),
		EventType: invitationEventType,
		Properties: map[string]string{
			"account":  user.ID(),
			"greeting": "hello-world",
		},
	}

	link, err := didcomm.GetInviteLink(reqBody)
	if err != nil {
		return nil, err
	}

	response := make(map[string]interface{})
	response["qrCodeLink"] = link

	return link, nil
}

func DeleteDevice(ctx *gin.Context, e cmn.Env) (any, error) {
	did := ctx.Param("id")
	_, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	didcomm := services.GetDIDComm(nil)

	err = didcomm.DeleteConnection(did)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func BlockDevice(ctx *gin.Context, e cmn.Env) (any, error) {
	did := ctx.Param("id")
	_, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}
	didcomm := services.GetDIDComm(nil)

	err = didcomm.BlockConnection(did)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func AddDevice(ctx *gin.Context, e cmn.Env) (any, error) {
	mock := make(map[string]string)
	mock["name"] = "My Device"
	mock["connectionId"] = "4729423498hsdfndfj3jsjsj"
	mock["pairingDate"] = "12.12.23"
	mock["qr"] = "aGVsbG8gd29ybGQ="
	return mock, nil
}
