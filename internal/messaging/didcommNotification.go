package messaging

import (
	"fmt"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"

	"github.com/cloudevents/sdk-go/v2/event"
)

func HandleDIDCommNotification(e event.Event, env common.Env) error {
	fmt.Println(string(e.Data()))
	return nil
}
