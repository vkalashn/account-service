package messaging

import (
	"context"
	"errors"
	"github.com/Nerzal/gocloak/v13"
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/libraries/messaging"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/handlers"
)

func HandlePresentationRequest(e event.Event, env common.Env) error {
	typ := common.RecordEventType(e.Type())
	if typ != common.PresentationRequest {
		return errors.ErrUnsupported
	}
	var record messaging.HistoryRecord
	err := e.DataAs(&record)
	if err != nil {
		logger.Error(err, "could not unpack event", "type", typ, "id", e.ID())
		return err
	}
	ctx := &gin.Context{}
	ctx.Params = append(ctx.Params, gin.Param{Key: "id", Value: record.RequestId})
	ctx.Request = ctx.Request.WithContext(context.WithValue(context.Background(), common.UserKey, &common.UserInfo{UserInfo: &gocloak.UserInfo{Sub: &record.UserId}}))
	var handler = func() error {
		_, er := handlers.GetPresentationRequest(ctx, env)
		return er
	}
	return handler()
}
