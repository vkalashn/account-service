package model

import (
	"fmt"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gorm.io/gorm"
)

const tableSchema = "accounts"

var logger = common.GetLogger()

func setSchema(db *gorm.DB) *gorm.DB {
	return db.Exec(fmt.Sprintf("SET search_path TO %s", tableSchema))
}
