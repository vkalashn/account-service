package model

import (
	"gorm.io/gorm"
)

type PresentationRequest struct {
	*gorm.Model
	UserId    string `gorm:"user_id"`
	RequestId string `gorm:"request_id"`
}

func CreatePresentationRequestDBEntry(db *gorm.DB, userId string, requestId string) error {
	request := PresentationRequest{UserId: userId, RequestId: requestId}
	sq := setSchema(db).Create(&request)
	return sq.Error
}

func GetAllPresentationRequests(db *gorm.DB, userId string) ([]PresentationRequest, error) {
	var requests []PresentationRequest
	err := setSchema(db).
		Where("user_id=?", userId).
		Find(&requests).Error
	return requests, err
}

func DeletePendingRequest(db *gorm.DB, userId string, requestId string) error {
	return setSchema(db).
		Where("user_id=? AND request_id=?", userId, requestId).
		Delete(&PresentationRequest{}).Error
}
