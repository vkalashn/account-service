package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/ssi/oid4vip/model/presentation"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/config"
	"net/http"
	"sync"
	"time"
)

var credentialVerification *CredentialVerification

var onceCredentialVerification sync.Once

func GetCredentialVerification(client common.HttpClient) *CredentialVerification {
	onceCredentialVerification.Do(initCredentialVerification(client))
	return credentialVerification
}

func initCredentialVerification(client common.HttpClient) func() {
	return func() {
		credentialVerification = &CredentialVerification{url: config.ServerConfiguration.CredentialVerifier.Url, httpClient: client}
	}
}

type credentialVerificationEndpoint string

const (
	getProofRequest credentialVerificationEndpoint = "/%s/request"
	createProof     credentialVerificationEndpoint = "/%s/proof"
)

type CredentialVerification struct {
	url        string
	httpClient common.HttpClient
}

type PresentationRequest struct {
	Region                 string                              `json:"region"`
	Country                string                              `json:"country"`
	Id                     string                              `json:"id"`
	RequestId              string                              `json:"requestId"`
	PresentationDefinition presentation.PresentationDefinition `json:"presentationDefinition"`
	Presentation           []interface{}                       `json:"presentation"`
	RedirectUri            string                              `json:"redirectUri"`
	ResponseUri            string                              `json:"responseUri"`
	ResponseMode           string                              `json:"responseMode"`
	ResponseType           string                              `json:"responseType"`
	State                  string                              `json:"state"`
	LastUpdateTimeStamp    time.Time                           `json:"lastUpdateTimeStamp"`
	Nonce                  string                              `json:"nonce"`
	ClientId               string                              `json:"clientId"`
}

type Proof struct {
	Payload       []presentation.FilterResult
	SignNamespace string
	SignKey       string
	SignGroup     string
	HolderDid     string
}

func (cv *CredentialVerification) getEndpointUrl(endpoint credentialVerificationEndpoint, id string) string {
	route := string(endpoint)
	if id != "" {
		route = fmt.Sprintf(route, id)
	}
	return fmt.Sprintf("%s%s", cv.url, route)
}

func (cv *CredentialVerification) GetProofRequest(requestId string) (*PresentationRequest, error) {
	req, err := http.NewRequest(http.MethodGet, cv.getEndpointUrl(getProofRequest, requestId), nil)
	if err != nil {
		return nil, err
	}
	resp, err := cv.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	var res PresentationRequest
	return handleResponse(resp, &res)
}

func (cv *CredentialVerification) CreateProof(requestId string, filterResults []presentation.FilterResult, namespace string, group string, signKey string, didDoc *DidDocument) error {
	didBytes, err := json.Marshal(didDoc)
	if err != nil {
		return err
	}

	proof := Proof{
		Payload:       filterResults,
		SignNamespace: namespace,
		SignKey:       signKey,
		SignGroup:     group,
		HolderDid:     string(didBytes),
	}
	data, err := json.Marshal(proof)
	req, err := http.NewRequest(http.MethodPost, cv.getEndpointUrl(createProof, requestId), bytes.NewBuffer(data))
	if err != nil {
		return err
	}
	resp, err := cv.httpClient.Do(req)
	if err != nil {
		return err
	}
	var res NilType
	_, err = handleResponse(resp, &res)
	return err
}
