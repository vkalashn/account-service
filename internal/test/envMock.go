package test

import (
	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/stretchr/testify/mock"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core/types"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gorm.io/gorm"
)

type EnvObjMock struct {
	mock.Mock
}

func (env *EnvObjMock) IsHealthy() bool {
	return true
}

func (env *EnvObjMock) GetDB() *gorm.DB {
	args := env.Called()
	return args.Get(0).(*gorm.DB)
}

func (env *EnvObjMock) GetBroker(topic string) *cloudeventprovider.CloudEventProviderClient {
	args := env.Called()
	return args.Get(0).(*cloudeventprovider.CloudEventProviderClient)
}

func (env *EnvObjMock) GetCryptoProvider() types.CryptoProvider {
	args := env.Called()
	return args.Get(0).(types.CryptoProvider)
}

func (env *EnvObjMock) GetNamespace() string {
	return ""
}

func (env *EnvObjMock) AddBroker(topic string, handler func(e event.Event)) {}

func (env *EnvObjMock) GetRandomId() string {
	args := env.Called()
	return args.String(0)
}

func (env *EnvObjMock) GetHttpClient() common.HttpClient {
	args := env.Called()
	return args.Get(0).(common.HttpClient)
}
