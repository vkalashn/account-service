package main

import (
	"os"

	"github.com/cloudevents/sdk-go/v2/event"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/api"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/common"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/env"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/messaging"
	"gitlab.eclipse.org/eclipse/xfsc/personal-credential-manager-cloud/account-service/internal/middleware"
)

var logger = common.GetLogger()

func initConfig() {
	// Load Configuration
	err := config.LoadConfig()
	if err != nil {
		logger.Error(err, "")
		os.Exit(1)
	}
}

func main() {
	initConfig()
	envir := env.GetEnv()
	server := core.NewServer(envir)
	// broker subscriptions are added here
	registerBrokerHandlers(envir)
	// routes and middleware are added here
	server.Add(func(group *gin.RouterGroup) {
		baseGroup := group.Group("/api/accounts")
		baseGroup.Use(middleware.CheckExistenceAndGetUserData())
		baseGroup.Use(middleware.CreateCryptoKeysIfAccountIsNew())
		api.DeviceRoutes(baseGroup, envir)
		api.HistoryRoutes(baseGroup, envir)
		api.KmsRoutes(baseGroup, envir)
		api.SettingsRoutes(baseGroup, envir)
		api.CredentialRoutes(baseGroup, envir)
		api.PresentationsRoutes(baseGroup, envir)
		api.ConfiguationsRoutes(baseGroup, envir)
	})

	err := server.Run(config.ServerConfiguration.Port)
	if err != nil {
		panic(err)
	}
}

func registerBrokerHandlers(envir common.Env) {
	envir.AddBroker("remote.xxx", func(e event.Event) {
		messaging.HandleError(messaging.HandleDIDCommNotification, e, envir, nil)
		messaging.HandleError(messaging.HandleHistoryRecord, e, envir, nil)
		messaging.HandleError(messaging.HandlePresentationRequest, e, envir, nil)
		messaging.HandleError(messaging.HandleCreateKey, e, envir, func(err error) {
			logger.Error(err, "failure during create key event handling")
		})
	})
}
