include $(PWD)/.env
include $(PWD)/.env.credentials

docker-compose-run:
	docker compose -f deployment/docker/docker-compose.yml -p $(PROJECT_NAME) --env-file=.env rm --force --stop
	docker-compose -f deployment/docker/docker-compose.yml -p $(PROJECT_NAME) --env-file=.env --env-file=.env.credentials build --no-cache
	docker compose -f deployment/docker/docker-compose.yml -p $(PROJECT_NAME) --env-file=.env --env-file=.env.credentials up --detach

restart:
	docker compose -f deployment/docker/docker-compose.yml -p $(PROJECT_NAME) --env-file=.env rm --force --stop server
	docker-compose -f deployment/docker/docker-compose.yml -p $(PROJECT_NAME) --env-file=.env --env-file=.env.credentials build --no-cache server
	docker compose -f deployment/docker/docker-compose.yml -p $(PROJECT_NAME) --env-file=.env --env-file=.env.credentials up --detach server